package Honeywords;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 *
 * This header must remain with this class and all sections of code taken from or based on this class, as so to
 * identify the original creator.
 * Copyright (c) 2015 Nishant Shanbhag, Bevan Stephen, and Gareth Jones.
 *
 * File:                Honeywords.HoneywordsGeneratorAPI.java
 * Date created:        07/09/2015
 * Date last edited:    07/10/2015
 * Author:              Nishant Shanbhag    <nishy_boi@live.com>
 * Editor:              Gareth Jones        <Jones258@gmail.com>
 *
 * Description:         Honeywords.HoneyChecker is a class that checks password entries for Honeywords and also creates
 *                      the 'password.txt' and 'credits.txt' files and does the storing of user credentials.
 */

/**
 * Add the Client Side code here.
 * This should also contain the code for the users, which connect to the HoneywordsServer class
 * Must contain a simple Honeywords.GUI that has login & sign up buttons, when clicked brings up
 * the respective fields. For example, for sign up, there should be username, password,
 * and confirm password field with a button that says sign up or create account. For login,
 * there should just be username and password fields with a login button.
 * <p/>
 * Login Scenario:
 * The user enters their credentials into the Honeywords.GUI. We must first confirm that the username exists,
 * if username does not exist, then send an error message saying: "username or password is incorrect"
 * if username exists, then send the password to the Honeywords.PasswordHash class where it hashes the password
 * entered and then matches it with the stored password hashes to see if it is a honeyword or the
 * actual password. If it is the actual password then it should show a message saying: "successfully
 * logged in", if a honeyword then show a message saying: "access denied" and then shutdown the server
 * so no users can log in.
 * <p/>
 * Sign up Scenario:
 * The user enters their credentials (username, password, confirm password) into the Honeywords.GUI. We must first
 * check whether the username exists, if username exists then it should show an error message saying:
 * "username is already taken", if username does not exist, then it should proceed to create a unique salt
 * associated with that account and then hash the password and the salt and then it should prepend/append
 * the hashed salt to the hashed password then generate a honeyword and some tough nuts and store it in the
 * password file.
 */

public class HoneywordsGeneratorAPI implements ActionListener
{
	//private int clicks = 0;
	private JLabel label = new JLabel("");
	private StringModel<String> model = new StringModel<>();
	private JList<String> list = new JList<>(model);
	private JFrame frame = new JFrame();

	public HoneywordsGeneratorAPI()
	{
		// the clickable button
		JButton button = new JButton("Generate Honeywords");
		button.addActionListener(this);

		// the panel with the button and text
		JPanel panel = new JPanel();
		panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 10, 30));

		JScrollPane scroller = new JScrollPane(list);
		panel.add(scroller);

		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets( 5, 5, 5, 5 );

		int y = 0;
		int x = 0;

		y = 0;
		{
			x = 0;
			{
				c.weightx = 0;
				c.weighty = 0;

				c.gridx = x;
				c.gridy = y;

				panel.add(button, c);
			}
		}

		y++;
		{
			x = 0;
			{
				c.weightx = 1;
				c.weighty = 1;

				c.gridx = x;
				c.gridy = y;

				panel.add(scroller, c);
			}
		}
		//panel.add(button);
		//panel.add(list);
		//list.setPreferredSize(new Dimension(200, 200));
		//button.setMaximumSize(new Dimension(100, 10));

		// set up the frame and display it
		frame.add(panel, BorderLayout.CENTER);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Honeywords/GUI");
		frame.pack();
		frame.setVisible(true);
	}

	// create one Frame
	public static void main(String[] args)
	{
		new HoneywordsGeneratorAPI();
	}

	// process the button clicks
	public void actionPerformed(ActionEvent e)
	{
		//clicks++;
		ArrayList<String> listOfFiles = new ArrayList<>();
		HoneywordGenerator honeywordGenerator = new HoneywordGenerator(listOfFiles);
		//honeywordGenerator.generateNewBatchOfPasswords(20);

		int i = 0;

		model.clear();

		ArrayList<String> passwordBatch = new ArrayList<>();
		boolean passed = false;

		while( !passed )
			try
			{
				passwordBatch = honeywordGenerator.generateNewBatchOfPasswords(20);
				passed = true;
			}
			catch( StringIndexOutOfBoundsException exception )
			{
				System.out.println("error");
				passed = false;
			}

		for( String password : passwordBatch )
		{
			i++;
			//label.setText(label.getText() + "\r\n" + "password " + i + ": " + password);
			model.add("password " + i + ": " + password);
		}
		//label.setText("Number of clicks:  " + clicks);
	}
}
