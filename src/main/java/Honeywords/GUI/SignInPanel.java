package Honeywords.GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 *
 * This header must remain with this class and all sections of code taken from or based on this class, as so to
 * identify the original creator.
 * Copyright (c) 2015 Nishant Shanbhag, Bevan Stephen and Gareth Jones.
 *
 * File:                Honeywords.GUI.SignInPanel.java
 * Date created:        01/10/2015
 * Date last edited:    07/10/2015
 * Author:              Gareth Jones        <Jones258@gmail.com>
 * Editor:              Nishant Shanbhag    <nishy_boi@live.com>
 *
 * Description:         Honeywords.GUI.SignInPanel is a class that holds input fields for signing in.
 */

public class SignInPanel extends JPanel
{
	/**
	 * Input field that expects a username
	 */
	private JTextField input_Username;
	/**
	 * Input field that expects a password
	 */
	private JPasswordField input_Password;

	public void addActionListenerForInputs(ActionListener listener)
	{
		input_Username.addActionListener(listener);
		input_Password.addActionListener(listener);
	}

	public SignInPanel(LayoutManager layout, boolean isDoubleBuffered)
	{
		super(layout, isDoubleBuffered);
		setupPanel();
	}

	public SignInPanel(LayoutManager layout)
	{
		super(layout);
		setupPanel();
	}

	public SignInPanel(boolean isDoubleBuffered)
	{
		super(isDoubleBuffered);
		setupPanel();
	}

	public SignInPanel()
	{
		setupPanel();
	}

	/**
	 * Gets the username value that's stored in the textfield of {@code input_Username}.
	 *
	 * @return the username the user wishes to have as their username.
	 */
	public String getUsername()
	{
		return input_Username.getText();
	}

	/**
	 * Gets the password value stored in the textfield of {@code input_Password}.
	 * Note that we return {@code char[]} instead of {@code String} for security reasons -
	 * Because of Java's string-interning {@code String} values can be read from memory, making it possible to steal someones password by
	 * hacking the memory. This isn't an issue with {@code char[]}.
	 *
	 * @return a {@code char[]} array.
	 */
	public char[] getPassword()
	{
		return input_Password.getPassword();
	}

	public void setupPanel()
	{
		input_Username = new JTextField();
		input_Password = new JPasswordField();

		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.weightx = 0.5;
		c.weighty = 0.5;

		c.fill = GridBagConstraints.BOTH;

		int x = 0;
		int y = 0;

		y = 0;
		{
			x = 0;
			{
				c.weightx = 0;
				c.weighty = 0;

				c.gridx = x;
				c.gridy = y;

				this.add(new JLabel("username: "), c);
			}

			x++; //1
			{
				c.weightx = 0.5;
				c.weighty = 0;

				c.gridx = x;
				c.gridy = y;

				this.add(input_Username, c);
			}
		}

		y++; //1
		{
			x = 0;
			{
				c.weightx = 0;
				c.weighty = 0;

				c.gridx = x;
				c.gridy = y;

				this.add(new JLabel("password: "), c);
			}

			x++; //1
			{
				c.weightx = 0.5;
				c.weighty = 0;

				c.gridx = x;
				c.gridy = y;

				this.add(input_Password, c);
			}
		}

		y++; //1
		{
			x = 0;
			{
				c.weightx = 0;
				c.weighty = 1;

				c.gridx = x;
				c.gridy = y;

				this.add(Box.createHorizontalGlue(), c);
			}
		}

//		c.weightx = 0.5;
//		c.weighty = 0.0;
//		c.gridy = 1;
//		mainPanel.add( Box.createVerticalStrut( 10 ), c );
//
//		c.weightx = 0.5;
//		c.weighty = 0.5;
//		c.gridy = 2;
//		mainPanel.add( panelExampleList, c );
//
//		c.weightx = 0;
//		c.weighty = 0;
//		c.gridy = 3;
//		mainPanel.add( jPanel_InputPanel_And_Label, c );

		input_Password.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyTyped(KeyEvent e)
			{
				keyTyped_InputPassword(e);
				super.keyTyped(e);
			}
		});

	}

	public void keyTyped_InputPassword(KeyEvent e)
	{
		System.out.println("input_Password: text changed");
	}
}
