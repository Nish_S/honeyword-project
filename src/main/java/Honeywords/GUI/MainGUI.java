package Honeywords.GUI;

import Honeywords.HoneyChecker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * This header must remain with this class and all sections of code taken from or based on this class, as so to
 * identify the original creator.
 * Copyright (c) 2015 Nishant Shanbhag, Bevan Stephen and Gareth Jones.
 *
 * File:                Honeywords.GUI.SignUpPanel.java
 * Date created:        01/10/2015
 * Date last edited:    07/10/2015
 * Author:              Gareth Jones        <Jones258@gmail.com>
 * Editor:              Nishant Shanbhag    <nishy_boi@live.com>
 *                      Bevan Stephen       <bgstephen52@gmail.com>
 *
 * Description:         Honeywords.GUI.MainGUI is the main class of the application. It runs the whole application.
 */

public class MainGUI
{
	public Font fontTitle = new Font( "Verdana", Font.BOLD, 12 );
	public Font fontSubtitle = new Font( "Verdana", Font.BOLD, 10 );

	enum SignMode
	{
		NONE, SIGN_IN, SIGN_UP
	}

	enum PasswordIssue
	{
		NONE, TOO_SHORT, TOO_LONG, NO_NUMBERS,
	}

	/** The current {@code SignMode} that we're in */
	private SignMode currentSignMode;
	/** Panel being displayed. Assigned a panel based on the current {@code currentSignMode} */
	private JPanel panel_Mode;
	private JPanel panel_Main;

	private JLabel title;
	private JLabel subtitle;

	private SignInPanel panel_SignIn;
	private SignUpPanel panel_SignUp;

	private JButton button_SelectSignUp;
	private JButton button_SelectSignIn;
	private JButton button_Submit;

	private JFrame frame;

	private HoneyChecker checker;

	public MainGUI()
	{
		setupGUI();
	}

	public MainGUI(HoneyChecker checker)
	{
		this.checker = checker;
		setupGUI();
	}

	public void setCHecker( HoneyChecker checker )
	{
		this.checker = checker;
	}

	public void setupGUI()
	{
		frame = new JFrame();

		panel_SignIn = new SignInPanel();
		panel_SignUp = new SignUpPanel();
		panel_Main = new JPanel();

		panel_Mode = new JPanel();

		panel_SignIn.addActionListenerForInputs(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				MainGUI.this.buttonPressed_Submit(null);
			}
		});
		panel_SignUp.addActionListenerForInputs(new AbstractAction()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				MainGUI.this.buttonPressed_Submit(null);
			}
		});

		//the default layout manager for panels is "FlowLayout" which will screw up the size of the single panel this
		//panel will hold.
		panel_Mode.setLayout( new BorderLayout() );
//      {
//          panel_Mode.add( panel_SignUp );
//          panel_SignUp.validate();
//          panel_Mode.setMinimumSize( new Dimension( panel_SignUp.getWidth(), panel_SignUp.getHeight() ) );
//      }

		title = new JLabel( "Welcome" );
		{
			title.setFont( fontTitle );
			title.setAlignmentX( Component.CENTER_ALIGNMENT );
		}

		subtitle = new JLabel( "what do you want to do?" );
		{
			subtitle.setFont( fontSubtitle );
			subtitle.setAlignmentX( Component.CENTER_ALIGNMENT );
		}

		button_SelectSignIn = new JButton( "sign-in" );
		button_SelectSignUp = new JButton( "sign-up" );

		button_Submit = new JButton();
		{
			button_Submit.setPreferredSize( new Dimension( 200, 50 ) );
			button_Submit.setAlignmentX( Component.CENTER_ALIGNMENT );
			button_Submit.setVisible( false );

			button_Submit.addActionListener( new ActionListener()
			{
				@Override
				public void actionPerformed( ActionEvent e )
				{
					MainGUI.this.buttonPressed_Submit( e );
				}
			} );
		}

		JPanel panel_SignSelect = new JPanel();
		panel_SignSelect.setLayout( new BoxLayout( panel_SignSelect, BoxLayout.LINE_AXIS ) );
		{
			panel_SignSelect.setAlignmentX( Component.CENTER_ALIGNMENT );

			panel_SignSelect.add( button_SelectSignIn );
			panel_SignSelect.add( Box.createHorizontalStrut( 5 ) );
			panel_SignSelect.add( button_SelectSignUp );

			button_SelectSignIn.addActionListener( new ActionListener()
			{
				@Override
				public void actionPerformed( ActionEvent e )
				{
					MainGUI.this.buttonPressed_SelectSignIn( e );
				}
			} );
			button_SelectSignUp.addActionListener( new ActionListener()
			{
				@Override
				public void actionPerformed( ActionEvent e )
				{
					MainGUI.this.buttonPressed_SelectSignUp( e );
				}
			} );
		}

		JPanel panel_Upper = new JPanel(); //upper/header panel
		panel_Upper.setLayout( new BoxLayout( panel_Upper, BoxLayout.PAGE_AXIS ) );
		{
			panel_Upper.add( title );
			panel_Upper.add( subtitle );
			panel_Upper.add( panel_SignSelect );
		}

		JPanel panel_Lower = new JPanel(); //lower/footer panel
		panel_Lower.setLayout( new BoxLayout( panel_Lower, BoxLayout.PAGE_AXIS ) );
		{
			panel_Lower.add( button_Submit );
		}

//              panel_Main.setLayout( new BoxLayout( panel_Main, BoxLayout.PAGE_AXIS ) );
//              {
//                      panel_Main.add( title );
//                      panel_Main.add( subtitle );
//                      panel_Main.add( panel_SignSelect );
//                      panel_Main.add( Box.createVerticalGlue() );
//                      panel_Main.add( panel_Mode );
//                      panel_Main.add( Box.createVerticalStrut( 5 ) );
//                      panel_Main.add( button_Submit );
//                      panel_Main.add( Box.createVerticalGlue() );
//              }

		/**
		 * Remember:
		 *      gridx, gridy : the grid location of the component
		 *
		 *      weightx, weighty : the pixel ratio the component gets on resize. a component with "1" weightx will get
		 *                         "all" the pixels when you increase the width of the main panel. This is theoretical
		 *                         in that the actual number of pixels are shared with components relative their
		 *                         weights, but not consuming. if three components of weightx 1, 1, 0.5 are in a panel
		 *                         thats resized by x number of pixels, then both the first and second component grows
		 *                         by 1*x pixels, while component three grows by 0.5*x pixel
		 *
		 *      A weight of 0 means that the component doesn't resize, but in order for resizing to work correctly at
		 *      least one component must have a weight greater than 0 (doesn't have to be 1 - all 0s except for a
		 *      single 0.1 will is just as acceptable).
		 *
		 *      insets : these are borders. new Insets( top, left, bottom, right ) - Change these to add padding to a
		 *      component.
		 *
		 *      Note also the use of "y++" and "x++" - this is so if you want to add a component on the grid in between
		 *      components you don't have to manually change the assignments, since the next value of the changed grid
		 *      x/y's will be 1+current-x/y
		 *
		 *      for example if you wanted to add a component at 3y, the next component will be added at 4y, without any
		 *      changes being needed. Otherwise you'd have to change "y = 3" to "y = 4", which is a massive hassle when
		 *      you have multiple components.
		 */

		panel_Main.setLayout( new GridBagLayout() );
		GridBagConstraints c = new GridBagConstraints();

		c.fill = GridBagConstraints.BOTH;

		int x = 0;
		int y = 0;

		y = 0;
		{
			x = 0;
			{
				c.weightx = 0;
				c.weighty = 0;

				c.insets = new Insets( 5, 0, 0, 0 );

				c.gridx = x;
				c.gridy = y;

				panel_Main.add( panel_Upper, c );
			}
		}

		y++; //1
		{
			x = 0;
			{
				c.weightx = 1;
				c.weighty = 0;

				c.insets = new Insets( 5, 5, 5, 5 );

				c.gridx = x;
				c.gridy = y;

				panel_Main.add( panel_Mode, c );
			}
		}

		y++; //2
		{
			x = 0;
			{
				c.weightx = 0; //By having only one Glue between the panel_Mode and the lower/footer panel,
				c.weighty = 1; //the sign-up and sign-in panels will both be sized so that their input fields line up.

				c.insets = new Insets( 0, 0, 0, 0 );

				c.gridx = x;
				c.gridy = y;

				panel_Main.add( Box.createVerticalGlue(), c );
			}
		}

		y++; //3
		{
			x = 0;
			{
				c.weightx = 0;
				c.weighty = 0;

				c.insets = new Insets( 0, 0, 5, 0 );

				c.gridx = x;
				c.gridy = y;

				panel_Main.add( panel_Lower, c );
			}
		}

		frame.setMinimumSize( new Dimension( 300, 230 ) );
		//frame.setPreferredSize( new Dimension( 1000, 500 ) );
		frame.setTitle( "Honeywords Honeywords.GUI" );
		frame.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
		frame.add( panel_Main );
		frame.pack();
		frame.setLocationRelativeTo( null );
		frame.setVisible( true );
	}

	public void buttonPressed_SelectSignIn( ActionEvent e )
	{
		button_SelectSignIn.setEnabled( false );
		button_SelectSignUp.setEnabled( true );

		setMode( SignMode.SIGN_IN );
	}

	public void buttonPressed_SelectSignUp( ActionEvent e )
	{
		button_SelectSignIn.setEnabled( true );
		button_SelectSignUp.setEnabled( false );

		setMode( SignMode.SIGN_UP );
	}

	public void buttonPressed_Submit( ActionEvent e )
	{
		switch( currentSignMode )
		{
			case NONE:
				//do nothing
				break;

			case SIGN_IN:
			{
				trySignIn();
			}
			break;

			case SIGN_UP:
			{
				trySignUp();
			}
			break;
		}
	}

	public void signIn_Success()
	{
		JOptionPane.showMessageDialog(frame, "Sign in successful",
				"Signed in successfully",
				JOptionPane.INFORMATION_MESSAGE);
	}

	public void signIn_Failed()
	{
		JOptionPane.showMessageDialog(frame, "Sign in failed (bad username/password)",
				"Signed in failed",
				JOptionPane.ERROR_MESSAGE);
	}

	public void trySignIn()
	{
		if( panel_SignIn.getUsername().equals("") )
		{
			JOptionPane.showMessageDialog( frame, "No username given",
					"No username provided",
					JOptionPane.ERROR_MESSAGE );

			return;
		}

		System.out.println( "trying to sign-in with [username: " + panel_SignIn.getUsername() + ", password: " + Arrays.toString(panel_SignIn.getPassword()) + "]" );

		checker.tryLogin(panel_SignIn.getUsername(), panel_SignIn.getPassword());
	}

	public void trySignUp()
	{
		ArrayList<PasswordIssue> issues = getPasswordIssues( panel_SignUp.getPassword() );
		String issueString = "";

		//ArrayList<PinIssues> problems = getPasswordIssues( panel_SignUp.getPin() );

		for( PasswordIssue issue : issues )
		{
			switch( issue )
			{
				case TOO_SHORT:
					issueString += "\t" + "password too short" + "\r\n";
					break;
				case TOO_LONG:
					issueString += "\t" + "password too long" + "\r\n";
					break;
				case NO_NUMBERS:
					issueString += "\t" + "password doesn't have a number" + "\r\n";
					break;
			}
		}

		if( !issueString.equals( "" ) )
		{
			JOptionPane.showMessageDialog( frame, "Invalid password: " + "\r\n" + issueString,
					"Invalid password",
					JOptionPane.ERROR_MESSAGE );

			return;
		}

		if( !doPasswordsMatch(panel_SignUp.getPassword(), panel_SignUp.getPasswordCheck()) )
		{
			JOptionPane.showMessageDialog( frame, "Passwords don't match",
					"Passwords don't match",
					JOptionPane.ERROR_MESSAGE );

			return;
		}

		if( panel_SignUp.getUsername().equals( "" ) )
		{
			JOptionPane.showMessageDialog( frame, "No username given",
					"No username provided",
					JOptionPane.ERROR_MESSAGE );

			return;
		}

		if( isUsernameAlreadyInUse(panel_SignUp.getUsername()))
		{
			JOptionPane.showMessageDialog( frame, "Username already in use",
					"Username already in use",
					JOptionPane.ERROR_MESSAGE );

			return;
		}

		if( panel_SignUp.getPin().equals( "" ) || panel_SignUp.getPin().length() != 4 )
		{
			JOptionPane.showMessageDialog( frame , "Invalid pin",
					"Invalid pin",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		/*String newPIN = "";
		newPIN = (String) JOptionPane.showInputDialog( frame,
													   "Enter PIN",
													   "Enter PIN",
													   JOptionPane.PLAIN_MESSAGE,
													   null,
													   null,
													   panel_SignUp.getPin() );

		while ( validPIN( newPIN ) )
		{
			newPIN = (String) JOptionPane.showInputDialog( frame,
														   "Invalid PIN",
														   "Enter PIN",
														   JOptionPane.ERROR_MESSAGE,
														   null,
														   null,
														   panel_SignUp.getPin() );
		}*/


		System.out.println( "trying to sign-up with [username: " + panel_SignUp.getUsername() + ", password: " + Arrays.toString(panel_SignUp.getPassword()) + ", check-password: " + Arrays.toString(panel_SignUp.getPasswordCheck()) + ", pin: " + panel_SignUp.getPin() + "]" );

		checker.tryCreateAccount(panel_SignUp.getUsername(), panel_SignUp.getPassword(), panel_SignUp.getPin());
	}

	/*public boolean validPIN(String pin)
	{
		return ( pin.length() != 4 || pin.equals( "" ));
	}*/

	public boolean isUsernameAlreadyInUse( String username )
	{
		return (checker == null || checker.isUsernameInUse(username));
	}

	/**
	 * Checks the given password for any issues, creating an {@code ArrayList} of {@code PasswordIssue}s that's
	 * returned for use.
	 * To create a new password condition, add a new issue to the {@code PasswordIssue} enumerator, then add the
	 * conditional checks here.
	 * The new issue should be added to the {@code issues} {@code ArrayList} if the conditional check is not meet.
	 *
	 * @param password the password to check.
	 *
	 * @return an {@code ArrayList} of all the {@code PasswordIssue}s with this {@code password}.
	 */
	public ArrayList<PasswordIssue> getPasswordIssues( char[] password )
	{
		ArrayList<PasswordIssue> issues = new ArrayList<>();
		boolean hasNumbers = false;

		//Checking password length
		if( password.length < 5 )
			issues.add( PasswordIssue.TOO_SHORT );
		else if( password.length > 40 )
			issues.add( PasswordIssue.TOO_LONG );

		//------------------------------------------

		int numberCount = 0;
		int minNumberRequirements = 1;

		//Checking password has at least one number
		for( char c : password )
		{
			if( Character.isDigit( c ) )
				numberCount++;

			if( numberCount >= minNumberRequirements )
				hasNumbers = true;
		}

		if( !hasNumbers )
			issues.add( PasswordIssue.NO_NUMBERS );

		//------------------------------------------

		return issues;
	}

	/**
	 * Compares two {@code char[]} passwords to see if they match.
	 * This is done by first checking that their lengths match, before iterating and comparing actual {@code Characters}.
	 *
	 * @param firstPassword  the password to be compared to
	 * @param secondPassword the password to compare
	 *
	 * @return {@code true} if the passwords match, otherwise {@code false}
	 */
	public boolean doPasswordsMatch( char[] firstPassword, char[] secondPassword )
	{
		if( firstPassword.length == secondPassword.length )
		{
			for( int i = 0; i < firstPassword.length; i++ )
			{
				if( firstPassword[i] != secondPassword[i] )
					return false;
			}

			return true;
		}

		return false;
	}

	public void setMode( SignMode mode )
	{
		currentSignMode = mode;

		switch( mode )
		{
			case NONE:
			{
				panel_SignIn.setVisible( false );
				panel_SignUp.setVisible( false );
				button_Submit.setVisible( false );
				button_Submit.setText( "none" );
			}
			break;

			case SIGN_IN:
			{
				setModePanel( panel_SignIn );
				button_Submit.setVisible( true );
				button_Submit.setText( "sign-in" );
			}
			break;

			case SIGN_UP:
			{
				setModePanel( panel_SignUp );
				button_Submit.setVisible( true );
				button_Submit.setText( "sign-up" );
			}
			break;
		}
	}

	public void setModePanel( JPanel panel )
	{
		panel_Mode.removeAll();
		panel_Mode.add( panel );
		//panel_Mode.validate();
		panel_Main.validate();
		panel_Main.repaint();

	}

	public static void main (String[] args)
	{
		HoneyChecker checker = new HoneyChecker();
		MainGUI gui = new MainGUI(checker);

		if( Files.notExists(HoneyChecker.PASS_PATH) )
		{
			try
			{
				Files.createFile(HoneyChecker.PASS_PATH);
			}
			catch( FileAlreadyExistsException x )
			{
				System.err.format("File password.txt already exists!");
			}
			catch( IOException x )
			{
				System.err.format("createFile error: %s%n", x);
			}
		}

		if( Files.notExists(HoneyChecker.USER_PATH) )
		{
			try
			{
				Files.createFile(HoneyChecker.USER_PATH);
			}
			catch( FileAlreadyExistsException x )
			{
				System.err.format("File credits.txt already exists!");
			}
			catch( IOException x )
			{
				System.err.format("createFile error: %s%n", x);
			}
		}

	}
}