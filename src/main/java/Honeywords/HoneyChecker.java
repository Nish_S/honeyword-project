package Honeywords;

import Honeywords.GUI.SignInPanel;

import javax.swing.*;
import java.io.IOException;
import java.nio.file.*;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * This header must remain with this class and all sections of code taken from or based on this class, as so to
 * identify the original creator.
 * Copyright (c) 2015 Nishant Shanbhag, Bevan Stephen,  and Gareth Jones.
 *
 * File:                Honeywords.HoneyChecker.java
 * Date created:        07/09/2015
 * Date last edited:    07/10/2015
 * Author:              Nishant Shanbhag    <nishy_boi@live.com>
 * Editor:              Bevan Stephen       <bestephen52@gmail.com>
 *                      Gareth Jones        <Jones258@gmail.com>
 *
 * Description:         Honeywords.HoneyChecker is a class that checks password entries for Honeywords and also creates
 *                      the 'password.txt' and 'credits.txt' files and does the storing of user credentials.
 */

/**
 * First the credentials go to the Honeywords.HoneyChecker class and the username is checked to see if user exists in
 * the 'credits.txt' file.
 * <p/>
 * If the user exists, then the unique salt is obtained and prepended to the password, which is then sent to the
 * Honeywords.PasswordHash class where it gets hashed and gets sent to the Honeywords.HoneyChecker. If user does not
 * exist, then an error message is shown and the user is prompted to re-enter their credentials.
 * The Honeywords.HoneyChecker then checks if the password hash saved and the one entered matches. If they do then
 * the user is allowed access. If it is a typo then the user is prompted to re-enter the credentials. If it is a
 * honeyword then the 2nd authentication method is enabled where they get 1 more try. If the 2nd authentication fails
 * (another honeyword is entered) then the account is locked (currently locks the whole system).
 * <p/>
 * Methods:
 * <p/>
 * create_index = ( ( ( salt / 2 ) - 8 ) + ( salt / 2 ) / 4 ) )
 * <p/>
 * This index could be stored in the 'credits.txt' file so that we can match the correct password with the user.
 * 'credits.txt' File: UID | username | salt | index
 * This index could be used to verify users since it is calculated from the salt and each salt is unique for each user.
 * (the unique salt could be derived from system time + date as it will always be unique)
 * <p/>
 * check_index(salt, index);
 * <p/>
 * update_index();
 * <p/>
 * get_index();
 * <p/>
 * Another problem, if the password is not in the file, then it could be a mistype or a guessing attack is occurring.
 * How do we differentiate between this? We don't want to raise an alarm for a mistype.
 * <p/>
 * public static int check_index(salt, index)
 * {
 *      IF salt is in indices
 *          THEN User exists
 *      ELSE
 *          THEN User does not exist
 *      END
 * <p/>
 *      IF index matches
 *          THEN user is authenticated
 *      ELSE IF honeyword was submitted
 *          THEN 2nd Authentication
 *      ELSE IF 2nd Autentication fails
 *          THEN lock account
 *      END
 * <p/>
 *      return indices[salt] == index
 * }
 * <p/>
 * public static int update_index(salt, index)
 * {
 *      indices[salt] = index //Add new salt/index pairing to dictionary
 * }
 * <p/>
 * public static void main( String[] args )     //Setup, register functions and then start running
 * {
 *      Honeywords.HoneyChecker = new Honeywords.HoneyChecker... //create a new instance of the Honeywords.HoneyChecker
 *      Honeywords.HoneyChecker.register_function(check_index, check_index);
 *      Honeywords.HoneyChecker.register_function(update_index, update_index);
 * }
 */

public class HoneyChecker
{
	public static final String PASSWORD_PATH = "password.txt";
	public static final Path PASS_PATH = Paths.get(PASSWORD_PATH);
	public static final String CREDITS_PATH = "credits.txt";
	public static final Path USER_PATH = Paths.get(CREDITS_PATH);

	private JPanel panel_SignIn;
	private JFrame frame;

	public static final int MAX_STIKES = 1;

//	public static boolean adminLoginAvailable = true;
//	public static boolean userLoginAvailable = true;

	public int strikes = 1;
	public String lastUsernameTried = "";

	public HashMap<String, Boolean> accountLocks = new HashMap<>();

	public Scanner pass_in;
//	{
//		try
//		{
//			pass_in = new Scanner(PASS_PATH);
//		}
//		catch( IOException e )
//		{
//			e.printStackTrace();
//		}
//	}

	public Scanner cred_in;
//	{
//		try
//		{
//			cred_in = new Scanner(USER_PATH);
//			System.out.println(cred_in.hasNext());
//		}
//		catch( IOException e )
//		{
//			e.printStackTrace();
//		}
//	}

	byte[] convertStringToBytes(String s)
	{
		byte[] bytes = s.getBytes();
		return bytes;
	}

	/**FIXED**/
	//@todo UIDs keep getting created as the same value, "E01". Need to fix this later
	private String createUID()
	{
		String newUID;
		int tempNum = getNumberOfAccounts() + 1;
		if( tempNum > 9 )
		{
			newUID = "E" + tempNum;
		}
		else
		{
			newUID = "E0" + tempNum;
		}
		return newUID;
	}

	/**
	 * Reads an entry from the 'credit.txt' file using the {@code cred_in} scanner.
	 * @return
	 */
	private Entry readEntryFromFile(int lineNumber)
	{
		Entry entry = new Entry();

		//System.out.println(cred_in.hasNext());
		try( Scanner accountScanner = new Scanner(USER_PATH);
		     Scanner passwordScanner = new Scanner(PASS_PATH); )
		{
			for( int i = 0; i < lineNumber; i++ )
			{
				if( accountScanner.hasNextLine() )
					accountScanner.nextLine();
				else
					break; //error

				if( passwordScanner.hasNextLine() )
					passwordScanner.nextLine();
				else
					break; //error
			}

			String userLine = accountScanner.nextLine();
			String passLine = passwordScanner.nextLine();
			entry.setupFromLine(userLine, passLine);
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}

		return entry;
	}

	private void createNewAccount(String username, char[] password, String pin)
	{
		Entry entry = new Entry();

		entry.setUsername(username);
		entry.setPin(pin);
	}

	private int getNumberOfAccounts()
	{
		int count = 0;

		/** try-catch-with-resource : handles closing the scanner regardless of what happens **/
		try( Scanner accountScanner = new Scanner(USER_PATH); )
		{
			while( accountScanner.hasNext() )
			{
				accountScanner.nextLine();
				count++;
			}
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}

		return count;// / 3;
	}

//	private void writeEntryToPasswordsFile(Entry entry)
//	{
//		for( int i = 0; i < entry.getNumberOfPasswords(); i++ )
//		{
//			byte[] passwords = convertStringToBytes(entry.getPassword(i) + "\r\n");
//
//			try
//			{
//				Files.write(PASS_PATH, passwords, StandardOpenOption.APPEND);
//			}
//			catch( IOException e )
//			{
//				e.printStackTrace();
//			}
//		}
//
//		//region Old code for using bPass1 number variables
//		//              byte[] bPass1 = convertStringToBytes(entry.getPassword1());
////              try {
////                      Files.write(PASS_PATH, bPass1, StandardOpenOption.APPEND);
////              } catch (IOException e) {
////                      e.printStackTrace();
////              }
////
////              byte[] bPass2 = convertStringToBytes(entry.getPassword2());
////              try {
////                      Files.write(PASS_PATH, bPass2, StandardOpenOption.APPEND);
////              } catch (IOException e) {
////                      e.printStackTrace();
////              }
////
////              byte[] bPass3 = convertStringToBytes(entry.getPassword3());
////              try {
////                      Files.write(PASS_PATH, bPass3, StandardOpenOption.APPEND);
////              } catch (IOException e) {
////                      e.printStackTrace();
////              }
////
////              byte[] bPass4 = convertStringToBytes(entry.getPassword4());
////              try {
////                      Files.write(PASS_PATH, bPass4, StandardOpenOption.APPEND);
////              } catch (IOException e) {
////                      e.printStackTrace();
////              }
////
////              byte[] bPass5 = convertStringToBytes(entry.getPassword5());
////              try {
////                      Files.write(PASS_PATH, bPass5, StandardOpenOption.APPEND);
////              } catch (IOException e) {
////                      e.printStackTrace();
////              }
////
////              byte[] bPass6 = convertStringToBytes(entry.getPassword6());
////              try {
////                      Files.write(PASS_PATH, bPass6, StandardOpenOption.APPEND);
////              } catch (IOException e) {
////                      e.printStackTrace();
////              }
////
////              byte[] bPass7 = convertStringToBytes(entry.getPassword7());
////              try {
////                      Files.write(PASS_PATH, bPass7, StandardOpenOption.APPEND);
////              } catch (IOException e) {
////                      e.printStackTrace();
////              }
//		//endregion
//	}

//	void writeEntryToAccountFile(Entry entry)
//	{
//		byte[] bUID = convertStringToBytes(entry.getUID() + "\r\n");
//		byte[] bUsername = convertStringToBytes(entry.getUsername() + "\r\n");
//		byte[] bSalt = convertStringToBytes(entry.getSalt() + "\r\n");
//
//		//System.out.println(Arrays.toString("string|string|string".split(",")));
//
//		try
//		{
//			Files.write(USER_PATH, bUID, StandardOpenOption.APPEND);
//			Files.write(USER_PATH, bUsername, StandardOpenOption.APPEND);
//			Files.write(USER_PATH, bSalt, StandardOpenOption.APPEND);
//		}
//		catch( IOException e )
//		{
//			e.printStackTrace();
//		}
//	}

//	void writeNextEntry(Entry entry)
//	{
//		writeEntryToAccountFile(entry);
//		writeEntryToPasswordsFile(entry);
//	}

	public boolean isUsernameInUse(String username)
	{
		//System.out.println( cred_in.hasNext() );
		Entry entry = new Entry();

		int numOfEntries = getNumberOfAccounts();

		for( int i = 0; i < numOfEntries; i++ )
		{
			entry = readEntryFromFile(i);

			if( entry.getUsername().equals(username) )
				return true;
		}

		return false;
	}

	/**
	 * Gets the starting line of an account in the accounts.txt file that has the given {@code username}.
	 * If no account with that {@code username} exists, {@code -1} is returned.
	 *
	 * @param username the username of the account we want to know the starting line of.
	 * @return line-number of the starting line of the account that has the given {@code username}, otherwise {@code -1}.
	 */
	private int getAccountIndexByUsername(String username)
	{
		//System.out.println( cred_in.hasNext() );
		Entry entry = new Entry();
		int numOfEntries = getNumberOfAccounts();

		for( int i = 0; i < numOfEntries; i++ )
		{
			/** FIXED **/
			/*
			@todo entry is just re-reading the first entry over and over again. It should read the first, and then read the next.
			idea for progression: alter close() functions so that they appear in another method, call that after the read
			function and then put those into one master read function
			 */

			entry = readEntryFromFile(i);
			System.out.println(entry.getUsername() + " / " + username);
			if( entry.getUsername().equals(username) )
			{
				return i;
			}
		}
		return -1;
	}

	public void tryLogin(String username, char[] password)
	{
//		if( !adminLoginAvailable )
//		{
//			/** error msg "System Facilities Suspended" **/
//			System.out.println("System Facilities Suspended");
//		}
//		else if ( !userLoginAvailable )
//		{
//			System.out.println("Account Facilities Suspended");
//		}
		if( accountLocks.containsKey("admin") || accountLocks.containsKey(username))
		{
			if ( accountLocks.containsKey("admin") )
			{
				System.out.println("System Locked - breach detected in Admin account");
			}
			else
			{
				System.out.println("Account Locked - Honeyword entered and wrong pin entered");
			}
		}
		else
		{
			int accountLine = getAccountIndexByUsername(username);
			Entry entry = new Entry();

			if( accountLine > -1 )
			{
//				for( int i = 0; i < accountLine; i++ )
//				{
//					System.out.println(i);
//					entry = readEntryFromFile(i);
//				}

				entry = readEntryFromFile(accountLine );

				login(entry, password);
			}
			else
			{
				JOptionPane.showMessageDialog(new JFrame(), "Account does not exist",
						"Account does not exist",
						JOptionPane.ERROR_MESSAGE);
				System.out.println("Account does not exist!");
			}
		}
	}

	/**
	 * Called when a honey word is entered in place of a password.
	 * @param triggeredEntry the user-account stored as an {@code Entry} that the honeyword was entered for.
	 */
	public void HoneywordTriggered(Entry triggeredEntry)
	{
		frame = new JFrame();
		panel_SignIn = new SignInPanel();

		String newPIN = "";
		newPIN = (String) JOptionPane.showInputDialog( frame,
													   "Enter PIN",
													   "Enter PIN",
													   JOptionPane.PLAIN_MESSAGE,
													   null,
													   null,
													   "" );

		while ( !validPIN( newPIN ) )
		{
			newPIN = (String) JOptionPane.showInputDialog(frame,
														  "Invalid PIN",
														  "Enter PIN",
														  JOptionPane.ERROR_MESSAGE,
														  null,
														  null,
														  "");
		}

		if( newPIN.equals( triggeredEntry.getPin() ) )
		{
			System.out.println("Honeyword entered but correct pin entered");

			JOptionPane.showMessageDialog( new JFrame(), "Sign in successful",
														 "Signed in successfully",
														 JOptionPane.INFORMATION_MESSAGE );
			System.out.println("login successful");
		}
		else
		{
			accountLocks.put(triggeredEntry.getUsername(), true);

			if ( accountLocks.containsKey("admin") )
			{
				System.out.println("System Locked - breach detected in Admin account");
			}
			else
			{
				System.out.println("Account Locked - Honeyword entered and wrong pin entered");
			}

			//System.out.println("Account Locked - Honeyword entered and wrong pin entered");
		}
//		else if( lastUsernameTried.equals("admin") )
//		{
//			//@todo add code here for admin lock
//			System.out.println("System LOCK DOWN - HONEYWORD USED");
//			adminLoginAvailable = false;
//		}
//		else
//		{
//			//@todo add code here for account lock
//			System.out.println("Account LOCK DOWN - HONEYWORD USED");
//			userLoginAvailable = false;
//		}
	}

	public boolean validPIN(String pin)
	{
		return pin != null && pin.length() == 4 && !pin.equals("");
	}

	void login(Entry entry, char[] password)
	{
		/** encrypt password using entry.salt **/
		String ePassword = "";

		lastUsernameTried = entry.getUsername();

		if( entry.hasPassword(String.valueOf(password)) )
		{
			if( entry.getIndexOfPassword(String.valueOf(password)) == entry.getIndexOfPassword() )
			{
				JOptionPane.showMessageDialog( new JFrame(), "Sign in successful",
						"Signed in successfully",
						JOptionPane.INFORMATION_MESSAGE );
				System.out.println("login successful");
			}
			else if( entry.getUsername().equals(lastUsernameTried) && strikes == MAX_STIKES)
			{
				/*JOptionPane.showMessageDialog( new JFrame(), "Sign in failed (bad username/password)",
						"Signed in failed",
						JOptionPane.ERROR_MESSAGE );*/

				HoneywordTriggered( entry );
			}
			else if( entry.getUsername().equals(lastUsernameTried) )
			{
				strikes++;
				JOptionPane.showMessageDialog( new JFrame(), "Sign in failed (wrong username or password)",
						"Signed in failed",
						JOptionPane.ERROR_MESSAGE );
				System.out.println("wrong password");

			}
		}
		else
		{
			JOptionPane.showMessageDialog( new JFrame(), "Sign in failed (wrong username or password)",
					"Signed in failed",
					JOptionPane.ERROR_MESSAGE );
			System.out.println("wrong password");
		}

//		if( Arrays.equals( passwords.get(entry.getIndexOfPassword()).toCharArray(), password))
//		{
//
//		}
//		else
//		{
//			System.out.println( "LOCK DOWN - HONEYWORD USED" );
//		}

//		if( Arrays.equals(entry.getPassword2(), password) )
//		{
//			/** status msg "Login successful!" **/
//			System.out.println("Login Successful!");
//		}
//		else if( Arrays.equals(entry.getPassword1(), password) ||
//				Arrays.equals(entry.getPassword3(), password) ||
//				Arrays.equals(entry.getPassword4(), password) ||
//				Arrays.equals(entry.getPassword5(), password) ||
//				Arrays.equals(entry.getPassword6(), password) ||
//				Arrays.equals(entry.getPassword7(), password) )
//		{
//			/** error msg "Honeyword detected! Login facilities suspended." **/
//			adminLoginAvailable = false;
//		}
//		else
//		{
//			System.out.println(Arrays.toString(entry.getPassword2()) + " / " + Arrays.toString(password));
//			System.out.println("Login failed: password incorrect");
//		}
	}

	public void tryCreateAccount(String username, char[] password, String pin)
	{
//		if( !adminLoginAvailable )
//		{
//			/** error msg "System Facilities Suspended" **/
//			System.out.println("System LOCK DOWN - Login not available");
//		}
//		else if ( !userLoginAvailable )
//		{
//			System.out.println("Account LOCK DOWN - Login not available");
//		}
		if( accountLocks.containsKey("admin") || accountLocks.containsKey(username))
		{
			if ( accountLocks.containsKey("admin") )
			{
				System.out.println("System Locked - breach detected in Admin account");
			}
			else
			{
				System.out.println("Account Locked - Honeyword entered and wrong pin entered");
			}
		}
		else
		{
			if( isUsernameInUse(username) )
			{
				System.out.println("username already in use");
			}
			else
			{
				createAccount(username, password, pin);
			}
//			boolean usernameTaken = isUsernameInUse(username);
//			            /*
//			            boolean passwordsMatch;
//
//                        if (password.equals(confirmPassword))
//                                passwordsMatch = true;
//                        else
//                                passwordsMatch = false;
//
//                        if (!passwordsMatch)
//                        {
//                                // error msg "passwords do not match"
//                                // don't run createAccount() until passwords match
//                        }
//                        else
//                        */
//			if( usernameTaken )
//			{
//
//				/** don't run createAccount() until username is not taken **/
//			}
//			else
//				createAccount(username, password);
		}
	}

	void writeEntry(Entry entry)
	{
		try
		{
			Files.write(USER_PATH, convertStringToBytes(entry.getEntryAsLine() + "\r\n"), StandardOpenOption.APPEND);
			Files.write(PASS_PATH, convertStringToBytes(entry.getPasswordsAsString() + "\r\n"), StandardOpenOption.APPEND);
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
	}

	//	void writeEntryToAccountFile(Entry entry)
//	{
//		byte[] bUID = convertStringToBytes(entry.getUID() + "\r\n");
//		byte[] bUsername = convertStringToBytes(entry.getUsername() + "\r\n");
//		byte[] bSalt = convertStringToBytes(entry.getSalt() + "\r\n");
//
//		//System.out.println(Arrays.toString("string|string|string".split(",")));
//
//		try
//		{
//			Files.write(USER_PATH, bUID, StandardOpenOption.APPEND);
//			Files.write(USER_PATH, bUsername, StandardOpenOption.APPEND);
//			Files.write(USER_PATH, bSalt, StandardOpenOption.APPEND);
//		}
//		catch( IOException e )
//		{
//			e.printStackTrace();
//		}
//	}

	void createAccount(String username, char[] password, String pin)
	{
		Entry entry = new Entry();

		entry.setUID(createUID());
		entry.setUsername(username);
		entry.setPin(pin);
		/** new honeyword generator **/
		/** new encryptor **/

		HoneywordGenerator hg = new HoneywordGenerator(new ArrayList<String>());
		PasswordHash ph = new PasswordHash();

		//int numOfPasswords = ThreadLocalRandom.current().nextInt(10, 15);
		int numOfPasswords = 10;
		int correctPasswordIndex = ThreadLocalRandom.current().nextInt(0, numOfPasswords);

		entry.setPasswordIndex( correctPasswordIndex );
		ArrayList<String> passwords = hg.generateNewBatchOfPasswords(numOfPasswords);

		try
		{
			System.out.println( "honeywords are: " );
			for( int i = 0; i < numOfPasswords; i++ )
			{
				if( i == correctPasswordIndex ) //0-bound, so password - 1
					//entry.addPassword(String.valueOf(password));
				entry.addPassword(PasswordHash.createHash(String.valueOf(password)));
				else
				{
				//System.out.println( i );
					System.out.println( "\t" + passwords.get(i));
					entry.addPassword(PasswordHash.createHash(passwords.get(i)));
					//entry.addPassword(passwords.get(i));
				}
			}
			PasswordHash.createHash("string");
		}
		catch( NoSuchAlgorithmException | InvalidKeySpecException e )
		{
			e.printStackTrace();
		}

		writeEntry(entry);

		JOptionPane.showMessageDialog(new JFrame(), "Account created",
				"Account created",
				JOptionPane.INFORMATION_MESSAGE);

//		/** store honeywords in these variables, with the exception of hPass2 that stores the real password **/
//		String hPass1 = "";
//		String hPass2 = ""; //not this one
//		String hPass3 = "";
//		String hPass4 = "";
//		String hPass5 = "";
//		String hPass6 = "";
//		String hPass7 = "";
//		/** confirm type of salt **/
//		String salt = "";
//		entry.setSalt(salt);
//
//		/** encrypt the above variables, storing the salt in entry.salt **/
//		hPass1 = "";
//		hPass2 = "";
//		hPass3 = "";
//		hPass4 = "";
//		hPass5 = "";
//		hPass6 = "";
//		hPass7 = "";
//
//		char[] array1 = hPass1.toCharArray();
//		entry.setPassword1(array1);
//		char[] array2 = hPass1.toCharArray();
//		entry.setPassword2(array2);
//		char[] array3 = hPass1.toCharArray();
//		entry.setPassword3(array3);
//		char[] array4 = hPass1.toCharArray();
//		entry.setPassword4(array4);
//		char[] array5 = hPass1.toCharArray();
//		entry.setPassword5(array5);
//		char[] array6 = hPass1.toCharArray();
//		entry.setPassword6(array6);
//		char[] array7 = hPass1.toCharArray();
//		entry.setPassword7(array7);
//
//		writeNextEntry(entry);
	}

	public static void main(String[] args) throws IOException
	{
		if( Files.notExists(PASS_PATH) )
		{
			try
			{
				Files.createFile(PASS_PATH);
			}
			catch( FileAlreadyExistsException x )
			{
				System.err.format("File password.txt already exists!");
			}
			catch( IOException x )
			{
				System.err.format("createFile error: %s%n", x);
			}
		}

		if( Files.notExists(USER_PATH) )
		{
			try
			{
				Files.createFile(USER_PATH);
			}
			catch( FileAlreadyExistsException x )
			{
				System.err.format("File credits.txt already exists!");
			}
			catch( IOException x )
			{
				System.err.format("createFile error: %s%n", x);
			}
		}
	}
}