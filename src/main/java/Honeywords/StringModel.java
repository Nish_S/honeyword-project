package Honeywords;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * This header must remain with this class and all sections of code taken from or based on this class, as so to
 * identify the original creator.
 * Copyright (c) 2015 Nishant Shanbhag, Bevan Stephen, and Gareth Jones.
 *
 * File:                Honeywords.PasswordHash.java
 * Date created:        05/10/2015
 * Date last edited:    07/10/2015
 * Author:              Nishant Shanbhag    <nishy_boi@live.com>
 * Editor:              Gareth Jones        <Jones258@gmail.com>
 *
 * Description:         Honeywords.StringModel is a class that is just a model for the file elements.
 */

public class StringModel<T> extends AbstractListModel
{
	private List<T> list;

	public StringModel()
	{
		this.list = new ArrayList<>();
	}

	@Override
	public int getSize()
	{
		return list.size();
	}

	@Override
	public Object getElementAt(int index)
	{
		return list.get(index);
	}

	public void add( T data )
	{
		list.add( data );
		fireIntervalAdded(this, 0, getSize());
	}

	public void clear()
	{
		list.clear();
		fireIntervalAdded(this, 0, getSize());
	}
}
