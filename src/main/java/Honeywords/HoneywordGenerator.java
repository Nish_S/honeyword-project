package Honeywords;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * Software licence is ''MIT License'':
 * Copyright (C) 2013 Ronald L. Rivest and Ari Juels.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * This header must remain with this class and all sections of code taken from or based on this class, as so to
 * identify the original creator.
 * Copyright (c) 2015 Gareth "G-Rath" Jones.
 *
 * File:                Honeywords.HoneywordsGenerator.java
 * Date created:        05/09/2015
 * Date last edited:    07/10/2015
 * Author:              Gareth Jones        <Jones258@gmail.com>
 *
 * Description:         Honeywords.HoneywordGenerator is a self-containing class that creates Honeywords from the
 *                      list of high probability passwords. Translated from Python to Java
 *                      from http://people.csail.mit.edu/rivest/honeywords/gen.py
 *                      by Gareth Jones.
 */

public class HoneywordGenerator
{
	public final static String asciiLowerCase = "abcdefghijklmnopqrstuvwxyz";

	//probabilities prob1, prob2, prob3 add up to 1 (heuristically chosen)
	public final static String asciiUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public final static String asciiLetters = asciiLowerCase + asciiUpperCase;
	public final static String asciiPunctuation = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~";
	public final static String asciiDigits = "0123456789";

	//syntax parameters for a password
	/** Probability that a generated password is a "tough nut" (password of length 40 of random chars). */
	private double probToughNut = 0.08;
	/** Chance of a "random" char at this position */
	private double prob1 = 0.10;
	/** chance of a markov-order-1 char at this position */
	private double prob2 = 0.40;
	/** choice of continuing copying from the same word */
	private double prob3 = 0.50;
	/** Add 3% noise words to the list of passwords */
	private double noisyWordPercent = 0.03;
	/** The minimum number of letters the password must have */
	private int minNumOfLetters = 1;
	/** The minimum number of digits the password must have */
	private int minNumOfDigits = 1;
	/** The minimum number of special (non-letter, non-digit) characters the password must have */
	private int minNumOfSpecials = 0;

	public int loops = 0;

	private ArrayList<String> highProbPasswords = new ArrayList<>( Arrays.asList( "123456",
	                                                                              "1234567",
	                                                                              "12345678",
	                                                                              "123asdf",
	                                                                              "Admin",
	                                                                              "admin",
	                                                                              "administrator",
	                                                                              "asdf123",
	                                                                              "backup",
	                                                                              "backupexec",
	                                                                              "changeme",
	                                                                              "clustadm",
	                                                                              "cluster",
	                                                                              "compaq",
	                                                                              "default",
	                                                                              "dell",
	                                                                              "dmz",
	                                                                              "domino",
	                                                                              "exchadm",
	                                                                              "exchange",
	                                                                              "ftp",
	                                                                              "gateway",
	                                                                              "guest",
	                                                                              "lotus",
	                                                                              "money",
	                                                                              "notes",
	                                                                              "office",
	                                                                              "oracle",
	                                                                              "pass",
	                                                                              "password",
	                                                                              "password!",
	                                                                              "password1",
	                                                                              "print",
	                                                                              "qwerty",
	                                                                              "replicate",
	                                                                              "seagate",
	                                                                              "secret",
	                                                                              "sql",
	                                                                              "sqlexec",
	                                                                              "temp",
	                                                                              "temp!",
	                                                                              "temp123",
	                                                                              "test",
	                                                                              "test!",
	                                                                              "test123",
	                                                                              "tivoli",
	                                                                              "veritas",
	                                                                              "virus",
	                                                                              "web",
	                                                                              "www",
	                                                                              "KKKKKKK" ) );

	private ArrayList<String> currentPasswordsList = new ArrayList<>();

	private Random rand = new Random();

	public HoneywordGenerator()
	{
		this( new ArrayList<String>() ); //pass a blank ArrayList
	}

	public HoneywordGenerator( ArrayList<String> fileList )
	{
		this( fileList.toArray(new String[fileList.size()]) );
	}

	public HoneywordGenerator( String... fileNames )
	{
		currentPasswordsList.addAll( readPasswordFiles( fileNames ) );
	}

	public static void main( String... args )
	{
		int numOfPasswordsToMake = 5;

		//String[] string = { "one", "two", "three"};
		//new Honeywords.HoneywordGenerator( string );
		//new Honeywords.HoneywordGenerator( "C:\\file.txt", "C:\\mypasswords\\passwords.txt" );


		ArrayList<String> listOfFiles = new ArrayList<>();

		for( int i = 0; i < args.length; i++ )
		{
			String arg = args[i];

			if( i == 0 ) //The first argument should be the number of passwords to create
			{
				numOfPasswordsToMake = Integer.parseInt( arg );
			}
			else //Read the password files passed
			{
				listOfFiles.add( arg );
			}
		}

		HoneywordGenerator generator = new HoneywordGenerator( listOfFiles );

		for( String password : generator.generateNewBatchOfPasswords( numOfPasswordsToMake ) )
		{
			System.out.println( "password: " + password );

		}

		System.out.println( "------" );
		System.out.println( "looped " + generator.loops + " times " );

	}

	public ArrayList<String> generateNewBatchOfPasswords( int batchSize )
	{
		return generatePasswordsUsingList( batchSize, currentPasswordsList );
	}

	public void addPasswordFiles( String... fileNames )
	{
		if( fileNames != null && fileNames.length > 0 ) //Prevent doubling up
			currentPasswordsList.addAll( readPasswordFiles( fileNames ) );
	}

	/**
	 * Return a list of passwords in all the password file(s), plus a proportional (according to parameter q) number of "noise" passwords.
	 *
	 * @param fileNames an "dynamic" static array of file names to read. These should be in full with both path and file extension.
	 *
	 * @return list of all the generated passwords
	 */
	private ArrayList<String> readPasswordFiles( String... fileNames )
	{
		ArrayList<String> passwords = new ArrayList<>();

		if( fileNames.length > 0 )
		{
			for( String fileName : fileNames )
			{
				try( BufferedReader bufferedReader = Files.newBufferedReader( Paths.get( fileName ), Charset.forName( "UTF-8" ) ) )
				{
					String line;
					while( ( line = bufferedReader.readLine() ) != null )
					{
						passwords.add( line );
					}
				}
				catch( IOException e )
				{
					e.printStackTrace();
				}
			}
		}
		else
		{
			passwords.addAll( highProbPasswords ); //"pw_list.extend( line.split() )" in Python
		}

		//"pw_list.extend( noise_list(int(q*len(pw_list)))
		passwords.addAll( createNoiseList( (int) ( noisyWordPercent * passwords.size() ) ) );

		return passwords;
	}

	private ArrayList<String> createNoiseList( int numberOfNoisyPasswordsToMake )
	{
		ArrayList<String> noisyPasswords = new ArrayList<>(); //"L" in Python

		String usableChars = asciiLetters + asciiDigits + asciiPunctuation; //"chars" in Python

		for( int i = 0; i < numberOfNoisyPasswordsToMake; i++ )
		{
			String password = ""; //"w = [ ]" in Python
			int randomLength = rand.nextInt( 18 ); //"k = random.randrange(1, 18)" in Python

			for( int j = 0; j < randomLength; j++ )
			{
				password += ( usableChars.charAt( rand.nextInt( usableChars.length() ) ) );
			}

			noisyPasswords.add( password );
		}

		return noisyPasswords;
	}

	/**
	 * Generates a "tough nut" password.
	 *
	 * @return a "tough nut" password
	 */
	private String generateToughNutPassword()
	{
		String usableChars = asciiLetters + asciiDigits + asciiPunctuation; //"chars" in Python
		String password = "";

		for( int i = 0; i < 40; i++ ) //"k = 40" and "for j in range(k)" in Python
		{
			password += usableChars.charAt( rand.nextInt( usableChars.length() ) );
		}

		return password;
	}

	/**
	 * Checks if the given password meets the minimal syntax requirements to be a usable honeyword password.
	 * <p/>
	 * The "minimal" syntax requirements are set by the three global variables {@link HoneywordGenerator#minNumOfLetters}, {@link HoneywordGenerator#minNumOfDigits}
	 * and {@link HoneywordGenerator#minNumOfSpecials} - If the number of each character type does not match up to the minimal threshold set by these variables
	 * then the password doesn't meet the minimal syntax requirement and so can't be used as a honeyword.
	 *
	 * @param password the password to check
	 *
	 * @return {@code true} if the given password meets the minimal requirements to be a honeyword, otherwise {@code false}
	 */
	private boolean doesPasswordMeetMinimalSyntax( String password )
	{
		int letterCount = 0;
		int digitCount = 0;
		int specialCount = 0;

		for( int i = 0; i < password.length(); i++ )
		{
			if( asciiLetters.indexOf( password.charAt( i ) ) > -1 )
			{
				letterCount++;
			}
			else if( asciiDigits.indexOf( password.charAt( i ) ) > -1 )
			{
				digitCount++;
			}
			else
			{
				specialCount++;
			}
		}

		//"if L >= nL and D >= nD and S >= nS: return True" in Python
		return letterCount >= minNumOfLetters && digitCount >= minNumOfDigits && specialCount >= minNumOfSpecials;
	}

	private String makePassword( ArrayList<String> passwordList )
	{
		loops++;
		String answer = "";

		if( rand.nextDouble() < probToughNut ) //"if random.random() < tn:" in Python
			return generateToughNutPassword();

		/** Start by picking a random password from the list - Save its length since we'll generate a new password of the same length. */
		int passwordLength = passwordList.get( rand.nextInt( passwordList.size() ) ).length(); //"k" in Python

		ArrayList<String> passwordsToUse = new ArrayList<>();

		//Create a list of all passwords that are of the same length as the password being passed
		for( String password : passwordList )
		{
			if( password.length() == passwordLength )
				passwordsToUse.add( password ); //"L = [ pw for pw in pw_list if len(pw) == k ]" in Python
		}

		int numOfPasswords = passwordsToUse.size(); //"nL = len(L)" in Python

		//Index of the random password being used
		int randomRow = rand.nextInt( numOfPasswords );

		//Start the answer with the first char of a random password
		answer += passwordsToUse.get( randomRow ).charAt( 0 );

		for( int i = 1; i < passwordLength; i++ )
		{
			int ranNum = rand.nextInt();
			String action = "";

			if( ranNum < prob1 )
				action = "action_1";
			else if( ranNum < prob1 + prob2 )
				action = "action_2";
			else
				action = "action_3";

			switch( action )
			{
				case "action_1":
				{
					// add same char that some random word of length k has in this position
					randomRow = rand.nextInt( numOfPasswords );
					answer += passwordsToUse.get( randomRow ).charAt( i );
					break;
				}

				case "action_2":
				{

					//<editor-fold desc="'LL = [ i for i in range(nL) if L[i][j-1]==ans[-1] ]' explained">
					/*
					Python explained:
							LL = [ i for i in range(nL) if L[i][j-1]==ans[-1] ]

						LL is a list, with the list being made up of a conditional loop (the code inside the [ ] ).
						L is another list, scoped from above. (Its the list of all passwords of length).

						range(nL) means "0 to nL"
						ans[-1] is accessing the string with a negative index.
							in Python a negative index accesses the array like a mirror:
								[-1] is the last element in the array
								[-2] is the second to last element in the array
								[-3] is the third to last element in the array
								... and so on

						That code does the following:
							loop the range of 0 to nL, assigning i the number of the current loop.
								if:
									the "[j-1]th" element of the element in L found at "i"
										IS EQUAL TO
									the last element ([-1]) of the answer string
								then:
									add "i" to the list
					*/ //</editor-fold>

					// take char in this position of random word with same previous char
					ArrayList<Integer> array = new ArrayList<>();

					for( int j = 0; j < numOfPasswords; j++ )
					{
						//System.out.println( "j: " + j + " | " + "size: " + passwordsToUse.size() + " | " + "length: " + answer.length() );
						System.out.println( passwordsToUse.get( j ) + " vs. " + answer );
						if( passwordsToUse.get( j ).charAt( i - 1 ) == answer.charAt( answer.length() - 1 ) )
							array.add( j );
					}

//					System.out.println( array.size() );
//					System.out.println( passwordLength + " | " + answer.length());
					randomRow = array.get( rand.nextInt( array.size() )) ;
					answer += passwordsToUse.get( randomRow ).charAt( i );
					break;
				}

				case "action_3":
				{
					//stick with same row, and copy another character
					answer += passwordsToUse.get( randomRow ).charAt( i );
					break;
				}
			}
		}

		//Be careful here - "nL" in Python is both global and local, so it might be an error to reference this here
		if( ( passwordLength > 0 || minNumOfDigits > 0 || minNumOfSpecials > 0 ) && !doesPasswordMeetMinimalSyntax( answer ) )
			return makePassword( passwordList );
		else
			return answer;
	}

	/**
	 * Generates {@code numOfPasswordsToGen} number of passwords.
	 *
	 * @param numOfPasswordsToGen the number of passwords to generate
	 * @param passwordList        list of passwords to use when generating the new passwords
	 *
	 * @return list of generated passwords
	 */
	private ArrayList<String> generatePasswordsUsingList( int numOfPasswordsToGen, ArrayList<String> passwordList )
	{
		ArrayList<String> passwords = new ArrayList<>();

		for( int i = 0; i < numOfPasswordsToGen; i++ )
		{
			String password = makePassword( passwordList );

			while( passwords.contains( password ) ) //Ensure each generated password is unique
			{
				password = makePassword( passwordList );
			}

			passwords.add( makePassword( passwordList ) );
		}

		return passwords;
	}
}