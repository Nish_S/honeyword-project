package Honeywords;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * This header must remain with this class and all sections of code taken from or based on this class, as so to
 * identify the original creator.
 * Copyright (c) 2015 Nishant Shanbhag, Bevan Stephen, and Gareth Jones.
 *
 * File:                Honeywords.Entry.java
 * Date created:        16/09/2015
 * Date last edited:    07/10/2015
 * Author:              Bevan Stephen       <bgstephen52@gmail.com>
 * Editor:              Nishant Shanbhag    <nishy_boi@live.com>
 *                      Gareth Jones        <Jones258@gmail.com>
 *
 * Description:         Honeywords.Entry is a class that acts as a custom datatype, used to structure the data within
 *                      each entry of the 'password.txt' and 'credits.txt' files.
 */
public class Entry
{
	private String UID;         //3 characters
	private String name;        //20 characters
	private int passwordIndex = -1;
	private ArrayList<String> passwords = new ArrayList<>();
	//private char[] password1;   //6 character min, 40 max
	//private char[] password2;
	//private char[] password3;
	//private char[] password4;
	//private char[] password5;
	//private char[] password6;
	//private char[] password7;
	private String salt;        //8 characters
	private Boolean lockedOut;
	private String pin;

	public boolean hasPassword(String password)
	{
		for( String s : passwords )
		{
			try
			{
				if( PasswordHash.validatePassword(password, s ) )
					//if( password.equals(s))
					return true;
				PasswordHash.createHash("string");

			}
			catch( NoSuchAlgorithmException | InvalidKeySpecException e )
			{
				e.printStackTrace();
			}
		}

		return false;
		//return passwords.contains(password);
	}

	public int getIndexOfPassword(String password)
	{
		for( int i = 0; i < passwords.size(); i++ )
		{
			String s = passwords.get(i);

			try
			{
				if( PasswordHash.validatePassword(password, s ) )
					//if( password.equals(s))
					return i;
				PasswordHash.createHash("string");
			}
			catch( NoSuchAlgorithmException | InvalidKeySpecException e )
			{
				e.printStackTrace();
			}
		}

		return -1;//passwords.indexOf(password);
	}

	/**
	 * Extracts data from two comma-delimited strings thats used for user-account and password information.
	 *
	 * @param userLine comma-delimited string that holds user-account information, such as the username and account salt
	 * @param passLine comma-delimited string that holds all the passwords for this account.
	 */
	public void setupFromLine(String userLine, String passLine)
	{
		String[] split = userLine.split(",");

		if( split.length < 4 )
			return; //@todo error

		UID = split[0];
		name = split[1];
		salt = split[2];
		passwordIndex = Integer.parseInt(split[3]);
		pin = split[4];

		Collections.addAll(passwords, passLine.split(","));
	}

	public int getIndexOfPassword()
	{
		return passwordIndex;
	}

	/**
	 * Generates a single comma-delimited string that holds all the user-account information for this {@code Entry}.
	 * @return comma-delimited list of user-account information
	 */
	public String getEntryAsLine()
	{
		return UID + "," + name + "," + salt + "," + passwordIndex + "," + pin;
	}

	/**
	 * Generates a single comma-delimited string that holds all the passwords for this {@code Entry}.
	 *
	 * @return comma-delimited list of passwords.
	 */
	public String getPasswordsAsString()
	{
		String line = "";
		for( int i = 0; i < passwords.size(); i++ )
		{
			String password = passwords.get(i);
			line += password + (i == passwords.size() - 1 ? "" : ",");
		}

		return line;
	}

	public void setUID(String s)
	{
		UID = s;
	}

	public void setUsername(String s)
	{
		name = s;
	}

	public void addPassword(String s)
	{
		passwords.add(s);
	}

	public void setSalt(String s)
	{
		salt = s;
	}

	public void setPin(String s)
	{
		pin = s;
	}

	//	public void setPassword1(char[] s)
//	{
//		password1 = s;
//	}
//	public void setPassword2(char[] s)
//	{
//		password2 = s;
//	}
//	public void setPassword3(char[] s)
//	{
//		password3 = s;
//	}
//	public void setPassword4(char[] s)
//	{
//		password4 = s;
//	}
//	public void setPassword5(char[] s)
//	{
//		password5 = s;
//	}
//	public void setPassword6(char[] s)
//	{
//		password6 = s;
//	}
//	public void setPassword7(char[] s)
//	{
//		password7 = s;
//	}

	public void setLockedOut(boolean b) { lockedOut = b; }

	public boolean getLockedOut() { return lockedOut; }

	public String getPin() { return pin; }

	public String getUID()
	{
		return UID;
	}

	public String getUsername()
	{
		return name;
	}

	public String getPassword(int index)
	{
		return passwords.get(index);
	}

	public int getNumberOfPasswords()
	{
		return passwords.size();
	}

	//	public char[] getPassword1() { return password1;}
//	public char[] getPassword2() { return password2; }
//	public char[] getPassword3() { return password3; }
//	public char[] getPassword4() { return password4; }
//	public char[] getPassword5() { return password5; }
//	public char[] getPassword6() { return password6; }
//	public char[] getPassword7() { return password7; }
	public String getSalt()
	{
		return salt;
	}

	public void setPasswordIndex(int passwordIndex)
	{
		this.passwordIndex = passwordIndex;
	}
	//http://www.java2s.com/Code/Java/Swing-JFC/AquickapplicationtoshowasimpleJLabel.htm
}